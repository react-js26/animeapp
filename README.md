## Mundo Anime (Anime App)

Es una aplicación con la que puedes buscar animes/mangas y recibir la siguiente información:
- Número de episodios
- Vídeo de Youtube
- Sinopsis
- Imagen de portada
- Nombre en japonés.

También puedes crear una lista de hasta 10 favoritos, puedes eliminar también de tus favoritos.

## Para utilizar

Usa el comando

### `npm start`

La aplicación estará en el modo desarrollo en\
[http://localhost:3000](http://localhost:3000) (Mira esto en tu explorador).


