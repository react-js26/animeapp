import React from 'react'

export const AnimeList = ({ animelist,setAnimeInfo,animeComponent,handleList}) => {
  const AddToList=animeComponent;
  return (
    <>
      {
        animelist ? (
          animelist.map((anime, index) => {
            return (
              <div className="card" key={index} onClick={()=>setAnimeInfo(anime)}>
                <img src={anime.attributes.posterImage.small} alt="animeImage" />
                <div className="anime-info">
                  <h4>{anime.attributes.titles.en_jp}</h4>
                  <h5 className='titulo'>Tipo: {anime.type}</h5>
                  <h5># de Episodios: {anime.attributes.episodeCount}</h5>
                  <div className="overlay" onClick={()=>handleList(anime)}>
                    <h3 className="center">{anime.attributes.titles.ja_jp}</h3>  
                    <div className="rating center"><h2>Rating:</h2><h3 className='center'>{anime.attributes.averageRating}</h3></div>
                    <div className="rating"><h2>Youtube:</h2><h3>¡Da click en la imagen!</h3></div>
                    <AddToList/>
                  </div>
                </div>
              </div>
            )
          })
        ) : "Sin resultados"
      }
    </>
  )
}