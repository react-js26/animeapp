import React from 'react'

export const AnimeInfo = (props) => {
  const {attributes:{titles:{en_jp},episodeCount, youtubeVideoId, synopsis}}=props.animeInfo
  return (
    <>
      <div className="anime-content">
        <h2>{en_jp}</h2><br />
        <div className="info">
          <h3># de Episodios: {episodeCount}</h3>
          <h3>Youtube: </h3> <a href={`https://www.youtube.com/watch?v=${youtubeVideoId}`}>Ver vídeo</a>
          <h3>Sinopsis: </h3>
          <p>{synopsis}</p> 
        </div>
      </div>
    </>
  )
}