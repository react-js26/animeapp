import React, { useEffect, useState } from "react";
import './componentes/style.css'
import { AnimeList } from "./componentes/listaAnime";
import { AnimeInfo } from "./componentes/InfoAnime";
import { AddToList } from "./componentes/AddLista";
import { RemoveFromList } from "./componentes/RemLista";

function App() {

  const [search,setSearch]=useState('Kimi')
  const [animeData,setAnimeData]=useState();
  const [animeInfo,setAnimeInfo]=useState()
  const [myAnimeList,setMyAnimeList]=useState([])

  const addTo=(anime)=>{
    const index=myAnimeList.findIndex((myanime)=>{
        return myanime.mal_id === anime.mal_id
    })
    if(index < 10){
      const newArray=[...myAnimeList,anime]
      setMyAnimeList(newArray);
    }
    
  }
  const removeFrom=(anime)=>{
    const newArray=myAnimeList.filter((myanime)=>{
      return myanime.mal_id !== anime.mal_id
    })
    setMyAnimeList(newArray)
  }
  const getData=async()=>{
    const res=await fetch(`https://kitsu.io/api/edge/anime?filter[text]=${search}`)
    const resData= await res.json();
    setAnimeData(resData.data)
    console.log(resData.data)
  }
  useEffect(()=>{
    getData()
  },[search])

  return (
    <>
      <div className="header">
        <h1>Mundo Anime</h1>
        <div>
          <input type="search" placeholder='¿Qué anime buscas?'
            onChange={(e)=>setSearch(e.target.value)}/>
        </div>
      </div>

      <div className="container">
        <div className="animeInfo">
          {animeInfo && <AnimeInfo animeInfo={animeInfo}/>}
        </div>
        <div className="anime-row">
          <h2 className="text-heading">Anime</h2>
          <div className="row">
            <AnimeList 
            animelist={animeData}
            setAnimeInfo={setAnimeInfo}
            animeComponent={AddToList}
            handleList={(anime)=>addTo(anime)}
            />
          </div>
          <h2 className="text-heading">Mis favoritos</h2>
          <div className="row">
            <AnimeList 
            animelist={myAnimeList}
            setAnimeInfo={setAnimeInfo}
            animeComponent={RemoveFromList}
            handleList={(anime)=>removeFrom(anime)}
            />
          </div>
        </div>
      </div>
    </>
  );
}
export default App;
